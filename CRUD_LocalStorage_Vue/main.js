const app = new Vue ({
  el: '#app',
  data: {
    titulo: 'GYM con Vue',
    tareas: [],
    item: ''
  },
  methods: {
    agregarTarea () {
      if(this.item != '')
      {
        this.tareas.push({
          nombre: this.item,
          estado: false
        });
        this.item = '';
        // Guarda información en local storage: (variable a usar, datos a guardar en formato Json)
        localStorage.setItem('gym-vue',  JSON.stringify(this.tareas));
      }
      else 
      {
        alert('Debe ingresar primero una tarea');
      }
    },
    editarTarea (index) {
      this.tareas[index].estado = true;
      localStorage.setItem('gym-vue', JSON.stringify(this.tareas));
    },
    eliminarTarea (index) {
      // splice se utiliza para eliminar de un array: array(#indice, valores a eliminar)
      this.tareas.splice(index, 1);
      localStorage.setItem('gym-vue', JSON.stringify(this.tareas));
    }
  },
  computed: {

  },
  // es una función que se ejecuta al cargar el archivo
  created () {
    /*
      Creamos una variable datosDB y obtenemos los valores del Local Storage: gym-vue
      local storage almacena variables Json, por lo que siempre debemos convertir la variable
      Comprobamos si existe la variable para almacenar esa información en la variable de Data
    */
    let datosDB = JSON.parse(localStorage.getItem('gym-vue'));
    if(datosDB === null){
      this.tareas = [];
    }else{
      this.tareas = datosDB;
    }
  },
})